from django.urls import path
from . import views, ajax

app_name = 'homepage'

urlpatterns = [
    path('', views.index, name='index'),
    path('result/', ajax.result, name='result'),
]
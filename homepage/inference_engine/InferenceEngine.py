from .utilities import *
from functools import reduce
from fractions import Fraction
from queue import SimpleQueue

import os, re

class InferenceEngine:
    def __init__(self, knowledge_based_dir_path=""):
        if (knowledge_based_dir_path == ""):
            knowledge_based_dir_path = os.path.dirname(
                os.path.abspath(__file__)) + "/Knowledge_Based/"

        self.relations = read_json(knowledge_based_dir_path + 'relations.json')
        self.rules = read_json(knowledge_based_dir_path + 'rules.json')
        self.patterns = read_json(knowledge_based_dir_path + 'patterns.json')
        self.filter = read_json(knowledge_based_dir_path + 'filter.json')
        self.maping_operators = {
            '+': lambda a: reduce(lambda x, y: x+y, a),
            '-': lambda a: reduce(lambda x, y: x-y, a),
            '*': lambda a: reduce(lambda x, y: x*y, a),
            '//': lambda a: reduce(lambda x, y: x//y, a),
            '/': lambda a: reduce(lambda x,y: x/y, a)
        }
        self.dictionary = []
        self.facts = []
        self.queries = []
        self.answer = ''
    
    def __repr__(self):
        return '<%s.%s object at %s>' % (
            self.__class__.__module__,
            self.__class__.__name__,
            hex(id(self))
        )

    def fit(self, input_str):
        self.dictionary = []
        self.facts = []
        self.queries = []
        self.answer = ''
        clauses = filter(lambda c: len(c), input_str.strip().replace(',', '.').split('.'))
        for clause in clauses:
            clause = clause.strip()
            check_question = "?" in clause or clause.lower().find("hỏi") != -1
            found = False
            for regex in self.patterns:
                if re.search(regex, clause.lower()) is not None:
                    found = True
                    relative = self.patterns[regex]
                    fact = self.getFact(relative, clause, check_question)
                    if fact is None:
                        return "Có lỗi trong phân tích dữ kiện của giả thiết \"%s\"." % (clause)
                    if check_question:
                        self.queries.append(fact)
                    else:
                        self.facts.append(fact)
                    break
            if not found:
                if check_question:
                    return "Không thể nhận dạng câu hỏi \"%s\"." % (clause)
                return "Không thể nhận dạng giả thiết \"%s\"." % (clause)
        self.infer()
        return self.answer

    def getFact(self, relative, clause, check_question=False):
        if relative not in self.relations:
            return None
        
        varPrototype = self.relations[relative]["var"]
        varSep = self.relations[relative]["split"]
        varFilter = self.relations[relative]["filter"] + self.filter["general"]
        for w in varFilter[:]:
            varFilter.append(w[0].upper()+w[1:])
        clause = re.sub("(%s)" % ("|".join(varFilter)), "", clause)

        num_regex = r"[-+]?\d*[\.\/]?\d+|\d+"
        numbers = re.findall(num_regex, clause)

        if check_question:
            clause = re.sub("(Hỏi|mấy|bao nhiêu|\?)", "", clause)
        
        objects = [
            re.sub("\\s+", " ", re.sub(num_regex, "", w.lower())).strip()
            for w in re.split("(%s)" % ("|".join(varSep)), clause)
        ]

        objects = list(filter(lambda w: all((len(w), re.match("\\s*$", w) is None, w not in varSep)), objects))

        varList = []

        for objects_ind in range(len(objects)):
            if objects[objects_ind] in self.dictionary: continue
            available = False
            for dictionary_ind in range(len(self.dictionary)):
                if self.dictionary[dictionary_ind].find(objects[objects_ind]) == 0:
                    objects[objects_ind] = self.dictionary[dictionary_ind]
                    available = True
                elif objects[objects_ind].find(self.dictionary[dictionary_ind]) == 0:
                    for ind in range(len(self.facts)):
                        for var_ind in range(len(self.facts[ind]['var'])):
                            if self.facts[ind]['var'][var_ind] == self.dictionary[dictionary_ind]:
                                self.facts[ind]['var'][var_ind] = objects[objects_ind]
                    self.dictionary[dictionary_ind] = objects[objects_ind]
                    available = True
            if not available:
                self.dictionary.append(objects[objects_ind])

        for v in range(len(varPrototype)-int(check_question)):
            if varPrototype[v].isupper():
                if len(objects) == 0: 
                    return None
                varList.append(objects.pop(0))
            else:
                if len(numbers) == 0:
                    return None
                tmp = re.split(r"(\/|\.)",numbers.pop(0))
                if len(tmp) == 1:
                    varList.append(Fraction(tmp[0]))
                elif tmp[1] == '/':
                    varList.append(Fraction(tmp[0], tmp[2]))
                else:
                    varList.append(Fraction(tmp[0]) + Fraction(tmp[2]))
        if check_question:
            varList.append(None)

        new_fact = {"name": relative, "var": varList}

        return new_fact
    
    def getStrictRule(self, i, j):
        F = self.facts
        varMap = dict()
        current_object = ord('A')
        current_number = ord('a')
        f1 = []
        for v in F[i]["var"]:
            if v is None:
                f1.append('_')
            elif type(v) is str:
                f1.append(chr(current_object))
                varMap[chr(current_object)] = v
                current_object += 1
            else:
                f1.append(chr(current_number))
                varMap[chr(current_number)] = v
                current_number += 1
        f2 = []
        if i == j:
            f2 = f1
        else:
            for v in F[j]["var"]:
                if v not in F[i]["var"]:
                    if v is None:
                        f2.append('_')
                    elif type(v) is str:
                        f2.append(chr(current_object))
                        varMap[chr(current_object)] = v
                        current_object += 1
                    else:
                        f2.append(chr(current_number))
                        varMap[chr(current_number)] = v
                        current_number += 1
                else:
                    tmp = f1[F[i]["var"].index(v)]
                    f2.append(tmp)
                    varMap[tmp] = v
        return varMap, "%s(%s)&%s(%s)" % (F[i]["name"], ';'.join(f1), F[j]["name"], ';'.join(f2))
    
    def getRestrictRule(self, i, j):
        F = self.facts
        varMap = dict()
        current_object = ord('A')
        current_number = ord('a')
        f1 = []
        for v in F[i]["var"]:
            if v is None:
                f1.append('_')
            elif type(v) is str:
                f1.append(chr(current_object))
                varMap[chr(current_object)] = v
                current_object += 1
            else:
                f1.append(chr(current_number))
                varMap[chr(current_number)] = v
                current_number += 1
        f2 = []
        for v in F[j]["var"]:
            if v is None:
                f2.append('_')
            elif type(v) is str:
                if v not in F[i]["var"]:
                    f2.append(chr(current_object))
                    varMap[chr(current_object)] = v
                    current_object += 1
                else:
                    tmp = f1[F[i]["var"].index(v)]
                    f2.append(tmp)
                    varMap[tmp] = v
            else:
                f2.append(chr(current_number))
                varMap[chr(current_number)] = v
                current_number += 1
            
        return varMap, "%s(%s)&%s(%s)" % (F[i]["name"], ';'.join(f1), F[j]["name"], ';'.join(f2))
        
    def infer(self):
        Q = self.queries
        F = self.facts
        R = self.rules
        ER = set() # Explored Rules
        EF = dict((str(f), []) for f in F) # mapping explored fact => predecessor facts
        FS = dict() # mapping consequent fact => its natural language inference
        RF = set() # Removed Facts
        question = len(Q)
        if question == 0:
            self.answer += "\nKhông tìm thấy câu hỏi!"
            return
        while question:
            match = False
            rule = None
            for i in range(len(F)):
                if str(F[i]) in RF: continue
                for j in range(len(F)):
                    if str(F[j]) in RF: continue
                    if str(F[i])+ "&" + str(F[j]) in ER:
                        continue
                    ER.add(str(F[i])+ "&" + str(F[j]))
                    Rules = [self.getStrictRule(i,j), self.getRestrictRule(i,j)]
                    for varMap, rule in Rules:
                        if rule in R:
                            match = True
                            new_facts = []
                            tmp = R[rule]
                            if None in F[i]["var"] + F[j]["var"] and F[i]["name"] == F[j]["name"]:
                                question -= 1
                                new_facts.append("$")

                            for fp in tmp["add"]:
                                new_fact = {
                                    "name": fp[: fp.index("(")],
                                    "var": fp[fp.index("(")+1: -1].split(';')
                                }
                                for ind in range(len(new_fact["var"])):
                                    v = new_fact["var"][ind]
                                    if v in varMap:
                                        new_fact["var"][ind] = varMap[v]
                                    else:
                                        func = self.maping_operators[v[:v.index('(')]]
                                        new_fact["var"][ind] = func([Fraction(varMap[item]) for item in v[v.index('(')+1:-1].split(',')])
                                new_facts.append(new_fact)
                            if all(str(new_fact) in EF for new_fact in new_facts):
                                continue
                            
                            for find_exception in tmp["remove"]:
                                if find_exception == 1:
                                    RF.add(str(F[i]))
                                elif find_exception == 2:
                                    RF.add(str(F[j]))
                            
                            desc = tmp["desc"]
                            for v in varMap:
                                if v.isupper():
                                    desc = desc.replace('$'+v, varMap[v])
                                else:
                                    desc = desc.replace('$'+v, str(Fraction(varMap[v])))
                            for operator_answer in [self.maping_operators[o[1:o.index('(')]](Fraction(t) for t in o[o.index('(')+1:-2].split(',')) for o in re.findall('\[.*\]', desc)]:
                                desc = re.sub('\[.*\]', str(Fraction(operator_answer)), desc)
                            self.answer += desc
                            for new_fact in new_facts:
                                FS[str(new_fact)] = desc
                                EF[str(new_fact)] = [str(F[i]), str(F[j])]
                                F.append(new_fact)
                            break
                    if match: break
                if match: break       
            if not match:
                if len(Q):
                    F.extend(Q)
                    Q = []
                    match = True
                else:
                    # self.answer += "\nCó lỗi trong quá trình giải!\n"
                    self.answer += ""
                    return
        
        tmpAnswer = ""
        q = SimpleQueue()
        q.put("$")
        visited = set()
        while not q.empty():
            u = q.get()
            if u in visited:
                continue
            visited.add(u)
            if u not in FS:
                self.answer += "\n\nCó lỗi trong quá trình tối giản lời giải!"
                return
            tmpAnswer = FS[u] + tmpAnswer
            for v in EF[u]:
                if v in EF and len(EF[v]) > 0:
                    q.put(v)
        self.answer = tmpAnswer

# if __name__ == "__main__":
#     while True:
#         engine = InferenceEngine()
#         print(engine.fit(input()))

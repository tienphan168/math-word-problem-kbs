from django.http import JsonResponse
from datetime import datetime
from .inference_engine import InferenceEngine
from .inference_engine.object import Engine

def result(request):
    question = request.GET.get('submit_text')
    time = datetime.now().strftime("%H:%M")

    engine = InferenceEngine()
    
    print(question)

    engine.fit(str(question))

    result = engine.answer

    if (result == ""):
        result = Engine(str(question))
        data = {
        'result': result.res,
        'time': time,
        }
    else:
        data = {
        'result': result,
        'time': time,
        }
    
    # print(result)

    return JsonResponse(data)


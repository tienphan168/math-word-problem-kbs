from django.shortcuts import render
from datetime import datetime

# Create your views here.
def index(request):
    now = datetime.now()
    time = now.strftime("%H:%M")
    context = {
        'time': time,
    }
    return render(request, 'homepage/index.html', context)